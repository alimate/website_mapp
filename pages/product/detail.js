// pages/product/detail.js
var util = require("../../utils/util.js");
var trail = require("../../utils/trail.js");
var html = require("../../utils/HtmlToNodes.js");
const app = getApp()

Page({

    /**
     * 页面的初始数据
     */
    data: {
        indicatorDots: false,
        autoplay: false,
        interval: 5000,
        duration: 1000,
        currentIndex: 1,
        screenWidth: 500,
        id: 0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        if (options.id) {
            this.setData({
                id: parseInt(options.id)
            })
        }
        var sysInfo = wx.getSystemInfoSync()
        //console.log(sysInfo)
        this.setData({
            screenWidth: sysInfo.windowWidth
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {
        wx.showLoading({
            title: '',
        })
        app.httpPost('product/view?id=' + this.data.id, json => {
            if (json.code == 1) {
                let data = json.data.product
                data.image = trail.fixImageUrl(data.image)
                json.data.images = trail.fixListImage(json.data.images, 'image')
                json.data.skus = trail.fixListImage(json.data.skus, 'image')
                if (data.content) {
                    //data.content = trail.fixContent(data.content)
                    data.content = html.HtmlToNodes(data.content, trail.fixTag)
                } else {
                    data.content = "<p class=\"emptycontent\">资料整理中</p>"
                }
                this.setData({
                    model: data,
                    skus: json.data.skus,
                    albums: json.data.images
                })
                app.initShare(this, data.title, data.img_url)
            }
            wx.hideLoading()
        })
    },
    bannerChange: function(e) {
        this.setData({
            currentIndex: e.detail.current
        })
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    }
})